#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: aspire-monitor-resolution-fix.sh
# 
#         USAGE: ./aspire-monitor-resolution-fix.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: this script use 3 arguments to work :
#              : 1: width resolution
#              : 2: height resolution
#              : 3: target monitor name
#              : 4: is position relative to reference monitor
#              : 5: reference monitor name
#              : more info see: man xrandr
#        AUTHOR: Hilal Hadyanto (hadyanto01@gmail.com), 
#     REFERENCE: https://gist.github.com/debloper/2793261
#  ORGANIZATION: 
#       CREATED: 12/18/2020 17:50
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
#===============================================================================
script_folder=/home/hianto/bin

width_res=$1
height_res=$2
target_monitor=$3
position=$4
reference_monitor=$5

modline=$(cvt $width_res $height_res | grep Modeline \
    | awk '{$1="";print}' | awk -F '"' '{$1="";print}')
modename=$(echo $modline | awk '{print $1}') 

echo $modename
#===============================================================================
xrandr --newmode $modline
xrandr --addmode $target_monitor $modename

xrandr --output $target_monitor --mode $modename

case $position in
    "left")
        xrandr --output $target_monitor --left-of $reference_monitor
        ;;

    "top"|"above")
        xrandr --output $target_monitor --above $reference_monitor
        ;;

    "right")
        xrandr --output $target_monitor --right-of $reference_monitor
        ;;

    "bottom"|"below")
        xrandr --output $target_monitor --below $reference_monitor
        ;;
esac    # --- end of case ---

if [ -n "$(pidof polybar)" ]; then
    notify-send "An instance of Polybar is running"
    killall polybar
    
    notify-send "Restarting Polybar to suit monitor configuration"
    $script_folder/launch_polybar
fi

if [ -f $HOME/.fehbg ]; then
    $HOME/.fehbg
fi

