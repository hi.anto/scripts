#!/usr/bin/env bash 
#===============================================================================
#
#          FILE: add-monitor-resolution.sh
# 
#         USAGE: ./add-monitor-resolution.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: this script use 3 arguments to work :
#              : 1: width resolution
#              : 2: height resolution
#              : 3: monitor name (see monitor name using xrandr command)
#              : man xrandr
#        AUTHOR: Hilal Hadyanto (hadyanto01@gmail.com), 
#     REFERENCE: https://gist.github.com/debloper/2793261
#  ORGANIZATION: 
#       CREATED: 12/18/2020 11:53
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
#===============================================================================
monitor_name=$3

modline=$(cvt $width_res $height_res | grep Modeline \
    | awk '{$1="";print}' | awk -F '"' '{$1="";print}')
modename=$(echo $modline | awk '{print $1}') 
# echo $modename

#===============================================================================
xrandr --newmode $modline
xrandr --addmode $monitor_name $modename

