#!/usr/bin/env bash
#
# Monitor setter using demnu + xrandr by Hillal Hadyanto

# Customizable Variable
# =============================================================================

script_folder=/home/hianto/bin
bspwm_configs=/home/hianto/.config/bspwm

normal_bg="#0c0d0e"
normal_fg="#fcfdfe"
selected_bg="#3182bd"
selected_fg="$normal_fg"

dmenu="dmenu -i -l 5\
    -nb $normal_bg\
    -nf $normal_fg\
    -sb $selected_bg\
    -sf $selected_fg"

choice1="laptop-external as Primary left of laptop-monitor"
choice2="laptop-external as Primary right of laptop-monitor"
choice3="laptop-external as Secondary left of laptop-monitor"
choice4="laptop-external as Secondary right of laptop-monitor"
choice5="laptop-external off"
choice6="laptop-external on laptop-screen off"

# DESKTOP_SESSION env variable are set on ~/.bashrc
# -----------------------------------------------------------------------------
spectrwm=$(which spectrwm | awk -F / '{print $NF}')
bspwm=$(which bspwm | awk -F / '{print $NF}')
xwindowmanager=$(echo $DESKTOP_SESSION)

# =============================================================================

source "$HOME/.my_hostnames"

selected=$(echo "$choice1
$choice2
$choice3
$choice4
$choice5
$choice6" | $dmenu -fn Noto\ Sans -p Atur\ Monitor\ External)

function ngasih_notif {
    arg1=$1
    
    notify-send "setting external monitor for $arg1"
    notify-send "setting $selected"
}

function pindah_desktop {
    local desktop_list_def_pri=(0 1 2 3 4)
    local desktop_list_def_sec=(5 6 7 8 9)

    local desktop_list_dual=($(bspc query -D --names))
    local desktop_list=($(bspc query -m $2 -D --names))

    case $1 in
        's')
            local x_desktop=$(bspc query -D --names | grep x)
            if [ "$x_desktop" = 'x' ]; then
                for desktop in ${desktop_list[@]}; do
                    bspc desktop $desktop -m $3;
                done
                bspc desktop $x_desktop -m $2
            else
                bspc monitor $2 -a x
            fi

            for desktop in ${desktop_list[@]}; do
                bspc desktop $desktop -m $3;
            done

            bspc monitor --reorder-desktops 0 1 2 3 4 5 6 7 8 9
            ;;
        'd')
            case "$(hostname)" in
                "$my_aspire_hostname")
                    for a in ${desktop_list_def_pri[@]}; do
                        for b in ${desktop_list_dual[@]}; do
                            if [ $a = $b ]; then
                                bspc desktop $b -m VGA1
                            fi
                        done
                    done

                    for a in ${desktop_list_def_sec[@]}; do
                        for b in ${desktop_list_dual[@]}; do
                            if [ $a = $b ]; then
                                bspc desktop $b -m LVDS1
                            fi
                        done
                    done
                    ;;
                "$my_ideapad_hostname")
                    for a in ${desktop_list_def_pri[@]}; do
                        for b in ${desktop_list_dual[@]}; do
                            if [ $a = $b ]; then
                                bspc desktop $b -m HDMI1
                            fi
                        done
                    done

                    for a in ${desktop_list_def_sec[@]}; do
                        for b in ${desktop_list_dual[@]}; do
                            if [ $a = $b ]; then
                                bspc desktop $b -m eDP1
                            fi
                        done
                    done
                    ;;
            esac
            for a in ${desktop_list_dual[@]}; do
                if [ $a = 'x' ]; then
                    bspc desktop x -r
                fi
            done
            ;;
    esac
}

case "$(hostname)" in
       $my_aspire_hostname)
           case $selected in
               $choice1)
                    ngasih_notif $my_aspire_hostname

                    xrandr --output LVDS1 --auto \
                        --output VGA1 --primary --auto --left-of LVDS1
                    pindah_desktop d
                    ;;
                $choice2)
                    ngasih_notif $my_aspire_hostname

                    xrandr --output LVDS1 --auto \
                        --output VGA1 --primary --auto --right-of LVDS1
                    pindah_desktop d
                    ;;
                $choice3)
                    ngasih_notif $my_aspire_hostname

                    xrandr --output LVDS1 --primary --auto \
                        --output VGA1 --auto --left-of LVDS1
                    pindah_desktop d
                    ;;
                $choice4)
                    ngasih_notif $my_aspire_hostname

                    xrandr --output LVDS1 --primary --auto \
                        --output VGA1 --auto --right-of LVDS1
                    pindah_desktop d
                    ;;
                $choice5)
                    ngasih_notif $my_aspire_hostname

                    xrandr --output LVDS1 --primary --auto \
                        --output VGA1 --off

                    pindah_desktop s VGA1 LVDS1
                    ;;
                $choice6)
                    ngasih_notif $my_aspire_hostname

                    xrandr --output VGA1 --primary --auto \
                        --output LVDS1 --off

                    pindah_desktop s LVDS1 VGA1
                    ;;
            esac
            ;;
       $my_ideapad_hostname)
           case $selected in
               $choice1)
                    ngasih_notif $my_ideapad_hostname

                    xrandr --output eDP1 --auto \
                        --output HDMI1 --primary --auto --left-of eDP1
                    pindah_desktop d
                    ;;
                $choice2)
                    ngasih_notif $my_ideapad_hostname

                    xrandr --output eDP1 --auto \
                        --output HDMI1 --primary --auto --right-of eDP1
                    pindah_desktop d
                    ;;
                $choice3)
                    ngasih_notif $my_ideapad_hostname

                    xrandr --output eDP1 --primary --auto \
                        --output HDMI1 --auto --left-of eDP1
                    pindah_desktop d
                    ;;
                $choice4)
                    ngasih_notif $my_ideapad_hostname

                    xrandr --output eDP1 --primary --auto \
                        --output HDMI1 --auto --right-of eDP1
                    pindah_desktop d
                    ;;
                $choice5)
                    ngasih_notif $my_ideapad_hostname

                    xrandr --output eDP1 --primary --auto \
                        --output HDMI1 --off

                    pindah_desktop s HDMI1 eDP1
                    ;;
                $choice6)
                    ngasih_notif $my_ideapad_hostname

                    xrandr --output HDMI1 --primary --auto \
                        --output eDP1 --off

                    pindah_desktop s eDP1 HDMI1
                    ;;
            esac
            ;;
esac

monitor_count=$(xrandr | grep \* | wc -l)

if [ "$selected" = "" ]; then
    notify-send "Nothing is selected canceling monitor setup"
    exit 1;
else
    if [ "$xwindowmanager" = $bspwm ]; then
        notify-send "You are running bspwm"
        notify-send "Restarting bspwm window rules to suit monitor configuration"
        if [ "$monitor_count" = '2' ]; then
            notify-send "Monitor count is now $monitor_count applying multihead window rules"
            . ${bspwm_configs}/bspwm_rules_dual
        else
            notify-send "Monitor count is now $monitor_count applying single monitor window rules"
            . ${bspwm_configs}/bspwm_rules
        fi
    fi

    if [ -n "$(pidof polybar)" ]; then
        notify-send "An instance of Polybar is running"
        killall polybar

        notify-send "Restarting Polybar to suit monitor configuration"
        $script_folder/launch_polybar
    fi
fi

