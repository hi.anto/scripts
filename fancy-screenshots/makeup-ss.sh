#!/usr/bin/env bash
#

# untuk mempercantik screenshot (mirip ss nya macOS) by Hilal Hadyanto
# terinspirasi dari Script Fungsi serupa buatan mas BanditHijo
#
# link:
#    https://www.gnu.org/software/gawk/manual/html_node/Field-Separators.html
#    https://stackoverflow.com/questions/1915726/rounded-corners-using-imagemagick-bakground-transparent-or-white/1916256#1916256
#    https://legacy.imagemagick.org/Usage/draw/
#    https://legacy.imagemagick.org/Usage/compose/
#    https://linuxize.com/post/bash-concatenate-strings/

# =============================================================================

# output-folder
# -----------------------------------------------------------------------------
folder_screenshot=/home/hianto/Pictures/screenshots

folder_ss_tmp=$folder_screenshot/.tmp
folder_ss_mask=$folder_screenshot/.mask

folder_ss_rounded=$folder_screenshot/rounded
folder_ss_shadow=$folder_screenshot/shadow
folder_ss_shadow_rounded=$folder_screenshot/shadow-rounded

folder_ss_solid_bg=$folder_screenshot/solid-bg
folder_ss_solid_bg_rounded=$folder_screenshot/solid-bg-rounded
folder_ss_solid_bg_author=$folder_screenshot/solid-bg-author
folder_ss_solid_bg_author_rounded=$folder_screenshot/solid-bg-author-rounded

folder_ss_gradient_bg=$folder_screenshot/gradient-bg
folder_ss_gradient_bg_rounded=$folder_screenshot/gradient-bg-rounded
folder_ss_gradient_bg_author=$folder_screenshot/gradient-bg-author
folder_ss_gradient_bg_author_rounded=$folder_screenshot/gradient-bg-author-rounded

# variable
# -----------------------------------------------------------------------------
color_profile="/usr/share/color/icc/colord/sRGB.icc"

border_size=4
border_size_rounded=8
background_color="white"
background_color_author="#EFEFEF"
background_size=16
shadow_size="76x16+0+16"
shadow_size_author="24x16+0+16"
gradient_angle=120
font_color_solid="#89847E"
font_color_gradient="#EFEFEF"
border_color="#2E2923"
gra_col1="#00B4DB"
gra_col2="#0083B0"
rad="12"

font="Fira-Mono-Bold"
font_size=16

date=$(date +%Y%m%d%H%M%S)
auth_pos="South"
auth_cord="+0+16"

# =============================================================================

# preparation
# -----------------------------------------------------------------------------
cd $folder_screenshot

rm -fr .mask .tmp

mkdir -p .mask .tmp
mkdir -p shadow rounded shadow-rounded
mkdir -p solid-bg solid-bg-author solid-bg-rounded solid-bg-author-rounded
mkdir -p gradient-bg gradient-bg-author gradient-bg-rounded gradient-bg-author-rounded

# file
# -----------------------------------------------------------------------------
daftar_ss=($(ls -p | grep -v /))
total_ss=$(echo ${#daftar_ss[@]})
ss_terakhir=$(echo ${daftar_ss[$(($total_ss-1))]})

# target-output
# -----------------------------------------------------------------------------
out_tmp=$folder_ss_tmp/tmp.png
out_mask=$folder_ss_mask/mask.png
out_tmp_bdr_round=$folder_ss_mask/mask-bdr-round.png
out_tmp_will_be_given_rounded_border=$folder_ss_tmp/tmp-will-be-border.png

out_rounded=$folder_ss_rounded/$ss_terakhir
out_shadow=$folder_ss_shadow/$ss_terakhir
out_shadow_rounded=$folder_ss_shadow_rounded/$ss_terakhir

out_solid=$folder_ss_solid_bg/$ss_terakhir
out_solid_rounded=$folder_ss_solid_bg_rounded/$ss_terakhir
out_solid_author=$folder_ss_solid_bg_author/$ss_terakhir
out_solid_author_rounded=$folder_ss_solid_bg_author_rounded/$ss_terakhir

out_gradient=$folder_ss_gradient_bg/$ss_terakhir
out_gradient_rounded=$folder_ss_gradient_bg_rounded/$ss_terakhir
out_gradient_author=$folder_ss_gradient_bg_author/$ss_terakhir
out_gradient_author_rounded=$folder_ss_gradient_bg_author_rounded/$ss_terakhir

# function
# -----------------------------------------------------------------------------
function cari_ukuran_gambar {
    local img=$1
    local x="x"

    ukuran_penuh=$(identify $img | awk '{print $3}')
    ukuran_lebar=$(identify $img | awk '{print $3}' | awk 'BEGIN {FS = "x"}; {print $1}')
    ukuran_tinggi=$(identify $img | awk '{print $3}' | awk 'BEGIN {FS = "x"}; {print $2}')

    ukuran_lebar_dengan_border=$(echo $(($ukuran_lebar+$border_size_rounded)))
    ukuran_tinggi_dengan_border=$(echo $(($ukuran_tinggi+$border_size_rounded)))
    ukuran_penuh_dengan_border="$ukuran_lebar_dengan_border$x$ukuran_tinggi_dengan_border"
}

function ngasih_border {
    local src=$ss_terakhir

    convert $src -bordercolor $border_color -border $border_size $out_tmp
}

function ngasih_rounded {
    local will_be_given_border=$1
    local src=$ss_terakhir
    local img=$out_tmp

    cari_ukuran_gambar $src

    convert -size $ukuran_penuh xc:none -draw "roundrectangle 0,0,$ukuran_lebar,$ukuran_tinggi,$rad,$rad" -colorspace sRGB $out_mask

    if [[ $will_be_given_border -eq 1 ]]; then
        convert $src -matte $out_mask -compose DstIn -composite $out_tmp_will_be_given_rounded_border
    else
        convert $src -matte $out_mask -compose DstIn -composite $img
    fi
}

function buat_rounded_border {
    local img=$ss_terakhir

    cari_ukuran_gambar $img

    convert -size $ukuran_penuh_dengan_border xc:none -fill $border_color \
        -draw "roundrectangle 0,0,$ukuran_lebar_dengan_border,$ukuran_tinggi_dengan_border,$rad,$rad" \
        -colorspace sRGB $out_tmp_bdr_round
}

function ngasih_bayangan {
    local for_author=$1
    local img=$out_tmp

	if [[ $for_author -eq 1 ]]; then
        convert $img \( +clone -background black -shadow $shadow_size_author \) +swap -background none -layers merge +repage $img
    else
        convert $img \( +clone -background black -shadow $shadow_size \) +swap -background none -layers merge +repage $img
    fi
}

function ngasih_bg {
    local for_author=$1
    local img=$out_tmp

	if [[ $for_author -eq 1 ]]; then
        convert $img -bordercolor $background_color_author -border $background_size $img
    else
        convert $img -bordercolor $background_color -border $background_size $img
    fi
}

function ngasih_gradien {
    local img=$out_tmp

    cari_ukuran_gambar $img

    convert $img \( -size $ukuran_penuh -define gradient:$gradient_angle gradient:$gra_col1-$gra_col2 \) +swap -flatten $img
}

function gabung_gambar {
    local t1=$1
    local t2=$2
    local img=$3

    composite -gravity center $t1 $t2 $img
}

# finishing-function
# -----------------------------------------------------------------------------
function ganti_profil_warna {
    local img=$1

    convert $out_tmp -profile $color_profile $img
}

# final-function
# -----------------------------------------------------------------------------

# transparent-bg
#
function pake_rounded {
    ngasih_rounded 0

    ganti_profil_warna $out_rounded
}

function pake_bdr_rounded {
    buat_rounded_border
    ngasih_rounded 1
    gabung_gambar $out_tmp_will_be_given_rounded_border $out_tmp_bdr_round $out_rounded

    ganti_profil_warna $out_rounded
}

function pake_shadow {
    ngasih_border
    ngasih_bayangan 0

    ganti_profil_warna $out_shadow
}

function pake_shadow_rounded {
    ngasih_rounded
    ngasih_bayangan 0

    ganti_profil_warna $out_shadow_rounded
}

function pake_shadow_bdr_rounded {
    buat_rounded_border
    ngasih_rounded 1
    gabung_gambar $out_tmp_will_be_given_rounded_border $out_tmp_bdr_round $out_tmp
    ngasih_bayangan 0

    ganti_profil_warna $out_shadow
}

# solid-bg
#
function pake_bg {
    ngasih_border
    ngasih_bayangan 0
    ngasih_bg 0

    ganti_profil_warna $out_solid
}

function pake_bg_rounded {
    ngasih_rounded
    ngasih_bayangan 0
    ngasih_bg 0

    ganti_profil_warna $out_solid_rounded
}

function pake_bg_bdr_rounded {
    buat_rounded_border
    ngasih_rounded 1
    gabung_gambar $out_tmp_will_be_given_rounded_border $out_tmp_bdr_round $out_tmp
    ngasih_bayangan 0
    ngasih_bg 0

    ganti_profil_warna $out_solid_rounded
}

function pake_bg_auth {
    ngasih_border
    ngasih_bayangan 1
    ngasih_bg 1

    convert $out_tmp -gravity $auth_pos -pointsize $font_size -fill $font_color_solid \
    -font $font -annotate $auth_cord "$USER : $date" $out_tmp

    ganti_profil_warna $out_solid_author
}

function pake_bg_auth_rounded {
    ngasih_rounded
    ngasih_bayangan 1
    ngasih_bg 1

    convert $out_tmp -gravity $auth_pos -pointsize $font_size -fill $font_color_solid \
    -font $font -annotate $auth_cord "$USER : $date" $out_tmp

    ganti_profil_warna $out_solid_author_rounded
}

function pake_bg_bdr_auth_rounded {
    buat_rounded_border
    ngasih_rounded 1
    gabung_gambar $out_tmp_will_be_given_rounded_border $out_tmp_bdr_round $out_tmp
    ngasih_bayangan 1
    ngasih_bg 1

    convert $out_tmp -gravity $auth_pos -pointsize $font_size -fill $font_color_solid \
    -font $font -annotate $auth_cord "$USER : $date" $out_tmp

    ganti_profil_warna $out_solid_author_rounded
}

# gradient-bg
#
function pake_gradien {
    ngasih_border
    ngasih_bayangan
    ngasih_gradien

    ganti_profil_warna $out_gradient
}

function pake_gradien_rounded {
    ngasih_rounded
    ngasih_bayangan
    ngasih_gradien

    ganti_profil_warna $out_gradient_rounded
}

function pake_gradien_bdr_rounded {
    buat_rounded_border
    ngasih_rounded 1
    gabung_gambar $out_tmp_will_be_given_rounded_border $out_tmp_bdr_round $out_tmp
    ngasih_bayangan 0
    ngasih_gradien

    ganti_profil_warna $out_gradient_rounded
}

function pake_gradien_auth {
    ngasih_border
    ngasih_bayangan 1
    ngasih_gradien

    convert $out_tmp -gravity $auth_pos -pointsize $font_size -fill $font_color_gradient \
    -font $font -annotate $auth_cord "$USER : $date" $out_tmp

    ganti_profil_warna $out_gradient_author
}

function pake_gradien_auth_rounded {
    ngasih_rounded
    ngasih_bayangan 1
    ngasih_gradien

    convert $out_tmp -gravity $auth_pos -pointsize $font_size -fill $font_color_gradient \
    -font $font -annotate $auth_cord "$USER : $date" $out_tmp

    ganti_profil_warna $out_gradient_author_rounded
}

function pake_gradien_bdr_auth_rounded {
    buat_rounded_border
    ngasih_rounded 1
    gabung_gambar $out_tmp_will_be_given_rounded_border $out_tmp_bdr_round $out_tmp
    ngasih_bayangan 1
    ngasih_gradien

    convert $out_tmp -gravity $auth_pos -pointsize $font_size -fill $font_color_gradient \
    -font $font -annotate $auth_cord "$USER : $date" $out_tmp

    ganti_profil_warna $out_gradient_author_rounded
}

